# set compiler
FC = gfortran

# set debugflag
DBGFLG = ON

# set compiler flags depending on debug setting
ifeq ($(DBGFLG), ON)
  FCFLAGS = -O0 -g -fcheck=all -Wall -Wno-unused-variable -fbacktrace -funroll-loops -Wno-unused-dummy-argument -Wno-conversion-extra
else
  FCFLAGS = -O3 -fbacktrace -funroll-loops
endif

# set .o files
OFILES = sizeof.o \
		globals.o \
		boundary.o \
		integration.o \
		fileio.o \
		shallow_water_1d.o

# general rule
%.o: ./%.f90
	$(FC) $(FCFLAGS) -c $< -o $@

# define the targets
transport: $(OFILES)
	$(FC) $(FCFLAGS) -o shallow_water_1d $(OFILES)
	rm *.o *.mod

all: $(OFILESINTEGRATION)
	$(FC) $(FCFLAGS) -o shallow_water_1d $(OFILES)
	rm *.o *.mod

# dependencies
shallow_water_1d.o: sizeof.o \
	globals.o \
	boundary.o \
	fileio.o \
	integration.o

intergation.o: globals.o

# cleaning
TEMP = shallow_water_1d *.o *.mod *.cbp
clean:
	rm -f $(TEMP)