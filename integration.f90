module integration
   ! module for various time integration implementations
   implicit none
   public

   double precision, dimension(:, :), allocatable :: q_RK3_low_storage, q_euler

   integer status

contains

   subroutine init_integration()

      use globals
      use boundary

      ! allocate integration auxiliaries
      allocate(q_euler(0:nx_max + 1, 1:2), stat=status)
      allocate(q_RK3_low_storage(0:nx_max+1, 1:2), stat=status)

   end subroutine init_integration

   subroutine RK3LowStorage(uu, hh)
      ! integrate using the Runge-Kutta low-storage method

      use globals

      double precision, dimension(0:nx_max+1) :: uu
      double precision, dimension(0:nx_max+1) :: hh


   end subroutine RK3LowStorage

   function rhs(uu, hh)

      use globals

      double precision, dimension(0:nx_max+1) :: uu
      double precision, dimension(0:nx_max+1) :: hh
      double precision, dimension(0:nx_max+1, 1:2) :: rhs
      double precision invdx


   end function rhs

end module integration
