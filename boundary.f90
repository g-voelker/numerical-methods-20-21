module boundary
   ! this module is used to deal with spatial boundary conditions

   use globals

   implicit none

   public

contains

   subroutine set_boundaries_periodic(yy)

      double precision, dimension(0:nx_max + 1) :: yy

      ! set periodic boundary conditions
      yy(0) = yy(nx_max)
      yy(nx_max + 1) = yy(1)

   end subroutine set_boundaries_periodic

end module boundary
