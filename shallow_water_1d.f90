program shallow_water_1d

   !-----------------------------------------------------------------------------
   ! - integrate the 1D invicid shallow water equations on a staggered grid
   ! - we use the following grid layout
   !
   !  nn  0   1   2   3   4       N-2 N-1  N  N+1
   !  uu  o---x---x---x---x-|  |---x---x---x---o
   !  hh    o---x---x---x---|  |-x---x---x---x---o
   !  ns    0   1   2   3       N-3 N-2 N-1  N  N+1
   !
   ! 'x' remark ordinary grid points, 'o' depicts ghost cells
   !
   ! - the x-axis is then xx = nn * dx with xmax = nn * dx * N
   ! - the staggered grid is given by xs = (nn + 0.5) * dx
   ! - the centered differences have to be applied accordingly
   !
   !-----------------------------------------------------------------------------

   use sizeof
   use globals
   use integration
   use boundary
   use fileio
   implicit none

   !declare all variables
   integer :: allocstat
   double precision, dimension(:), allocatable :: uu
   double precision, dimension(:), allocatable :: hh

   ! initialize system parameters for i/o
   call getsize

   ! set time step and number of time steps + 1
   ! take care that the first data point corresponds to the initial conditions
   ! parameters are now stored in globals module
   gg = 981d-1                ! gravity
   Href = 2d0                 ! reference height
   xx_max = 50d0              ! horizontal size of the domain in meter
   tt_max = 1                 ! maximum time in seconds
   cphase = (gg * Href)**5d-1 ! typical phase velocity
   nx_max = 500               ! number of points in x
   dx = xx_max / nx_max       ! horizontal grid spacing
   dt = 1d-1 * dx / cphase    ! set a time step depending on spatial discretization (CFL criterion)
   record = -1                ! initialize i/O record number


end program shallow_water_1d