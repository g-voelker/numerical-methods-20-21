module globals
   ! this module is used to store global constants for all subroutines and modules
   implicit none
   public

   ! global parameters
   double precision :: cphase        ! typical phase veolity
   double precision :: gg            ! gravity
   double precision :: Href          ! reference height
   double precision :: tt_max        ! number f time steps
   double precision :: xx_max        ! horizontal size of the domain
   double precision :: dx            ! spatial discretization
   double precision :: dt            ! time step
   integer nx_max                    ! number of points in x
   integer nx, nt                    ! iteration indices
   integer record                    ! record number for file io
   character (len=1024) :: filename  ! file name for io

contains

end module globals
