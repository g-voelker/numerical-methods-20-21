##
# header

# import statements
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as ani

# set layout template
plt.style.use('ggplot')

# set parameters
dtype = 'float64'
data_path = '../shallow_water_1d.dat'
save_path = 'shallow_water_1d.pdf'
output = 'shallow_water_1d.mp4'
save = True

# run parameters
gg = 9.81
Href = 2
xx_max = 25
tt_max = 1
cphase = (gg * Href)**.5
nx_max = 500
dx = xx_max / nx_max
dt = dx / cphase * 0.1


##
#  load data and reshape

data = np.fromfile(data_path, dtype=dtype)

# set number of time steps
nt_max = int(len(data) / 2 / (nx_max + 1)) - 1

data = data.reshape((nt_max + 1, 2, nx_max + 1))

xx = np.linspace(0, xx_max, nx_max + 1)
tt = np.linspace(0, tt_max, nt_max + 1)


##
#  plot data

fig, ax = plt.subplots(2, 1)

nt_skip = 1

for nn in range(0, 2):
    image = ax[nn].pcolorfast(tt[::nt_skip], xx, data[::nt_skip, nn].T, zorder=2)
    cbar = fig.colorbar(image, ax=ax[nn], label=r'$\phi(t, x)$')

    ax[nn].set_xlabel('time (s)')
    ax[nn].set_ylabel('distance (m)')

plt.tight_layout()

if save:
    plt.savefig(save_path)


##
# build an animation

# generate initial plot
fs = 14

fig, axes = plt.subplots(1, 1, figsize=(8, 6))

image, = axes.plot(data[0, 1])

axes.set_ylabel('$h$ (m)', fontsize=fs)
axes.set_xlabel('x (m)', fontsize=fs)

fig.suptitle('t = {0:2d}s'.format(0), fontsize=fs + 2)

fig.tight_layout(rect=[0, 0, 1, 1])


# draw frame function for update of plot
def drawframe(frame):

    frame *= 4
    image.set_ydata(data[frame, 1, :])

    fig.suptitle('frame = {0:4d}'.format(frame), fontsize=fs + 2)


#  set animation properties
animation = ani.FuncAnimation(fig, drawframe,
                              frames=len(data)//4,
                              interval=1,
                              repeat=False)

# save out the animation
if save:
    animation.save(output, fps=8, bitrate=-1)
    plt.close()
else:
    plt.show()

