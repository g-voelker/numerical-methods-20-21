module fileio
   ! this module is used to do the fileio

   use globals
   use sizeof

   implicit none

   public

contains

   subroutine tape_out(uu, hh)

      double precision, dimension(0:nx_max + 1) :: uu
      double precision, dimension(0:nx_max + 1) :: hh
      double precision, dimension(1:nx_max + 1) :: hs

      ! increment record number
      record = record + 2

      ! average hh to full grid
      do nx = 1, nx_max + 1
         hs(nx) = 5d-1 * (hh(nx) + hh(nx - 1))
      end do

      ! write uu and hs to file
      open(10, file = filename, form = "unformatted", access = "direct", recl = (nx_max + 1) * sizeofdouble)
      write(10, rec=record) uu(1:nx_max+1)
      write(10, rec=record + 1) hs(1:nx_max+1)
      close(10 )

   end subroutine tape_out

end module fileio
